#!/usr/bin/env python3

DB_HOST = "db_host"
DB_USER = "db_user"
DB_NAME = "db_name"
DB_PASS = "db_pass"
